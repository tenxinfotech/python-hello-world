#!/usr/bin/python3

# A hello world python program

import time

if __name__ == "__main__":
	while True:
		now = time.strftime("%Y-%m-%d %H:%M:%S")
		print("Hello world, I am running {}".format(now))
		time.sleep(1)
